
function reduce (l, f, a) {
  let result = a;
  for (const i of l) {
    result = f(result, i);
  }
  return result;
}

const reduceF = (l, f, a) => l.length === 0 ? a : reduceF(l.slice(1), f, f(a, l[0]));

const map = (l, f) => l.reduce((a, i) => a.concat(f(i)), []);

const filter = (l, f) => l.reduce((a, i) => f(i) ? a.concat(i) : a, []);

const some = (l, f) => l.reduce((a, i) => a || f(i), false);

const none = (l, f) => l.reduce((a, i) => a && !f(i), true);

const every = (l, f) => l.reduce((a, i) => a && f(i), true);

const flat = l => l.reduce((a, i) => a.concat(i), []);

const uniq = l => l.reduce((a, i) => a.includes(i) ? a : a.concat(i), []);

const find = (l, f) => l.reduce((a, i) => a === undefined && f(i) ? i : a, undefined);

const fromEntries = l => undefined;

const reverse = l => undefined;

const join = (l, s) => undefined;

const findIndex = (l, f) => undefined;

const regroup = (l, similar, merge) => undefined;

module.exports = {
  reduce,
  reduceF,
  map,
  filter,
  some,
  none,
  every,
  flat,
  uniq,
  find,
  fromEntries,
  reverse,
  join,
  findIndex,
  regroup,
}

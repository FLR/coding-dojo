const test = require('ava');

const {
  reduce,
  reduceF,
  map,
  filter,
  some,
  none,
  every,
  flat,
  uniq,
  find,
  fromEntries,
  reverse,
  join,
  findIndex,
  regroup,
} = require('./reduce');

test('reduce', t => {
  t.deepEqual(reduce([], (a, b) => a + b, 0), 0);
  t.deepEqual(reduce([1,2,3], (a, b) => a + b, 0), 6);
  t.deepEqual(reduce([{i: 4},{i: 5},{i: 6}], (a, b) => ({i: a.i + b.i}), {i: 0}), {i: 15});
});

test('reduceF', t => {
  t.deepEqual(reduceF([], (a, b) => a + b, 0), 0);
  t.deepEqual(reduceF([1,2,3], (a, b) => a + b, 0), 6);
  t.deepEqual(reduceF([{i: 4},{i: 5},{i: 6}], (a, b) => ({i: a.i + b.i}), {i: 0}), {i: 15});
});

test('map', t => {
  t.deepEqual(map([], i => i*2), []);
  t.deepEqual(map([1,2,3], i => i*2), [2,4,6]);
});

test('filter', t => {
  t.deepEqual(filter([], i => i%2 === 0), []);
  t.deepEqual(filter([1,2,3], i => i%2 === 0), [2]);
});

test('some', t => {
  t.deepEqual(some([], i => i%2 === 0), false);
  t.deepEqual(some([1,2,3], i => i%2 === 0), true);
  t.deepEqual(some([1,5,3], i => i%2 === 0), false);
});

test('none', t => {
  t.deepEqual(none([], i => i%2 === 0), true);
  t.deepEqual(none([1,2,3], i => i%2 === 0), false);
  t.deepEqual(none([1,5,3], i => i%2 === 0), true);
});

test('every', t => {
  t.deepEqual(every([], i => i%2 === 0), true);
  t.deepEqual(every([1,2,3], i => i%2 === 0), false);
  t.deepEqual(every([1,5,3], i => i%2 === 0), false);
  t.deepEqual(every([2,4,6], i => i%2 === 0), true);
});

test('flat', t => {
  t.deepEqual(flat([]), []);
  t.deepEqual(flat([[2,3],[],[7]]), [2,3,7]);
});

test('uniq', t => {
  t.deepEqual(uniq([]), []);
  t.deepEqual(uniq([1,2,3]), [1,2,3]);
  t.deepEqual(uniq([2,1,2,3,1]), [2,1,3]);
});

test('find', t => {
  t.is(find([], i => i === 2), undefined);
  t.is(find([1,2,3], i => i === 2), 2);
  t.is(find([1,4,3], i => i === 2), undefined);
  t.is(find([0,3], i => i % 2 === 0), 0);
});

test('fromEntries', t => {
  t.deepEqual(fromEntries([]), {});
  t.deepEqual(fromEntries([['un', 1], ['deux', 2]]), {un: 1, deux: 2});
});

test('reverse', t => {
  t.deepEqual(reverse([]), []);
  t.deepEqual(reverse([1,2,3]), [3,2,1]);
})

test('join', t => {
  t.deepEqual(join([], " "), "");
  t.deepEqual(join(["florian", "le", "roux"], " "), "florian le roux");
})

test('findIndex', t => {
  t.is(findIndex([], i => i === 2), -1);
  t.is(findIndex([1,2,3], i => i === 2), 1);
  t.is(findIndex([1,4,3], i => i === 2), -1);
});

test('regroup', t => {
  t.deepEqual(regroup([], (a, b) => a === b, (a, b) => a + b), []);
  t.deepEqual(
    regroup(
      [
        {label: "un", value: 42},
        {label: "deux", value: 43},
        {label: "un", value: 44},
      ],
      (a, b) => a.label === b.label,
      (a, b) => ({label: a.label, value: a.value + b.value})
    ), [
      {label: "un", value: 86},
      {label: "deux", value: 43},
    ]
  );
});


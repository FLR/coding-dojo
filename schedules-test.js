const test = require('ava');

const {schedulesIntersection} = require('./schedules');

test('intersection-no-schedule', t => {
  t.deepEqual(schedulesIntersection([]), []);
});

test('intersection-two-empty-schedule', t => {
  t.deepEqual(schedulesIntersection([[], []]), []);
});

test('intersection-one-empty-schedule', t => {
  t.deepEqual(schedulesIntersection([[[1,2]], []]), []);
});

test('intersection-no-overlapping-intervals', t => {
  t.deepEqual(schedulesIntersection([[[1,2]], [[3,4]]]), []);
});

test('intersection-two-overlapping-intervals', t => {
  t.deepEqual(schedulesIntersection([[[1,3]], [[2,4]]]), [[2,3]]);
});

test('intersection-overlapping-and-no-overlapping-intervals', t => {
  t.deepEqual(schedulesIntersection([[[1,3],[4,6]], [[2,5],[7,8]]]), [[2,3],[4,5]]);
});

test('intersection-more-intervals', t => {
  t.deepEqual(schedulesIntersection([[[1,5],[6,7]], [[2,4]], [[3,7]]]), [[3,4]]);
});
